#!/usr/bin/env python3

#TODO: même chose en revenant à un attr _queue interne
#TODO: check attributes names to raise exception on collision

import asyncio
from functools import wraps 

def chain_async_func(cls):
    cls._root_instances = set()
    _init = cls.__init__
    def __init(self, *args, **kwargs):
        self.__init_args   = args
        self.__init_kwargs = kwargs
        self._action = None
        self.prev = None
        cls._root_instances.add(self)
        _init(self, *args, **kwargs)
    cls.__init__ = __init
    def __stack_action(func):
        @wraps(func)
        def _(self, *args, **kwargs):
            self._action = func(self, *args, **kwargs)
            new = cls(*self.__init_args, **self.__init_kwargs)
            cls._root_instances.remove(self)
            new.prev = self
            return new
        return _
    for coro in cls.__stack__:
        setattr(cls, coro, __stack_action(getattr(cls,coro)))
    async def exec(self):
        if self.prev is not None:
            await self.prev._exec()
        if self._action is not None:
            await self._action
    cls._exec = exec
    def start(cls):
        loop = asyncio.get_event_loop()
        loop.run_until_complete(asyncio.wait(
            [ inst._exec() for inst in cls._root_instances ]
            ))
        loop.close
    cls.start = classmethod(start)
    return cls

@chain_async_func
class Hero:
    __stack__ = [ 'sleep', 'jump' ]
    
    def __init__(self, name):
        self.name  = name

    async def sleep(self, delay):
        print(f"{self.name} sleeping for {delay}s")
        await asyncio.sleep(delay)

    async def jump(self):
        print(f"{self.name} jumping")

if __name__ == '__main__':
    Hero('Mario').jump().sleep(2).jump()
    Hero('Luigi').jump().sleep(1).jump().jump()
    Hero('Peach').sleep(4).jump().sleep(1).jump().jump()
    Hero.start()

