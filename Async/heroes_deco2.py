#!/usr/bin/env python3

# to read:
# https://djangostars.com/blog/asynchronous-programming-in-python-asyncio/
# Merci à Julien pour l'inspiration et à Artiom pour le soutien moral

import asyncio
from functools import wraps

class Hero:
    def __init__(self, name):
        self.name = name
        self.queue = []

    def __add_to_queue(func):
        @wraps(func)
        def _(self, *args, **kwargs):
            self.queue.append(func(self, *args, **kwargs))
            return self
        return _

    @__add_to_queue
    async def sleep(self, delay):
        print(f"{self.name} sleeping for {delay}s")
        await asyncio.sleep(delay)

    @__add_to_queue
    async def jump(self):
        print(f"{self.name} is jumping")

    async def exec(self):
        # ou si on veut rejouer les actions
        # depuis le début :
        #for coro in self.queue:
        #    await coro
        while self.queue:
            await self.queue.pop(0)

if __name__ == '__main__':
    from event_loop import Event_loop
    Event_loop(
     Hero('Mario')
      .jump()
      .sleep(3)
      .jump()
      .sleep(1)
      .jump()
      .jump()
      .sleep(2)
      .jump()
      .exec()
      ,
     Hero('Luigi')
      .sleep(2)
      .jump()
      .jump()
      .sleep(6)
      .jump()
      .jump()
      .exec()
      )
