#!/usr/bin/env python3

import asyncio
from functools import wraps

class Hero:
    def __init__(self, name, prev=None):
        self.name   = name
        self.prev   = prev
        # How to avoid RunTimeWarning properly ?
        # (not masking it)
        self.action = self._nothing()

    def __stack_action(func):
        @wraps(func)
        def _(self, *args, **kwargs):
            self.action = func(self, *args, **kwargs)
            return Hero(self.name, prev=self)
        return _

    async def _nothing(self):
        print('Here _nothing is awaited')
        pass

    @__stack_action
    async def sleep(self, delay):
        print(f"{self.name} sleeping for {delay}s")
        await asyncio.sleep(delay)

    @__stack_action
    async def jump(self):
        print(f"{self.name} jumping")

    async def exec(self):
        if self.prev is not None:
            await self.prev.exec()
        # this is were _nothing() is awaited...
        # But Python couldn't know
        # RuntimeWarning: coroutine 'Hero._nothing' was never awaited
        await self.action

loop = asyncio.get_event_loop()
tasks = [ Hero('Mario').jump().sleep(1).jump().exec() ]
loop.run_until_complete(asyncio.wait(tasks))
loop.close() 
