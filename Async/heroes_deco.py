#!/usr/bin/env python3

# to read:
# https://djangostars.com/blog/asynchronous-programming-in-python-asyncio/
# Merci à Julien pour l'inspiration et à Artiom pour le soutien moral

import asyncio
from functools import wraps
from time import time

def add_to_queue(func):
    @wraps(func)
    def _(self, *args, **kwargs):
        self.queue.append(func(self,*args, **kwargs))
        return self
    return _

class Hero:
    def __init__(self, name):
        self.name = name
        self.queue = []

    @add_to_queue
    async def sleep(self, delay):
        print(f"{self.name} sleeping for {delay}s")
        await asyncio.sleep(delay)

    @add_to_queue
    async def jump(self):
        print(f"{self.name} is jumping")

    @add_to_queue
    async def die(self):
        print(f"{self.name} is dead :-(")

    @add_to_queue
    async def clock(self,n, d=1):
        start = time()
        for i in range(n):
            print(f'{self.name} ({d}s): {i}s, real: {time() - start:.2f}')
            await asyncio.sleep(d)

    async def exec(self):
        # ou si on veut rejouer les actions
        # depuis le début :
        #for coro in self.queue:
        #    await coro
        while self.queue:
            await self.queue.pop(0)

if __name__ == '__main__':
    from event_loop import Event_loop
    Event_loop(
     Hero('Clock:')
       .clock(10)
       .exec()
     , 
     Hero('Clock2:')
       .clock(10,2)
       .exec()
     , 
     Hero('\t\tMario')
      .jump()
      .sleep(3)
      .jump()
      .sleep(1)
      .jump()
      .jump()
      .sleep(2)
      .jump()
      .die()
      .exec()
      ,
     Hero('\t\t\t\t\tLuigi')
      .sleep(2)
      .jump()
      .jump()
      .sleep(6)
      .jump()
      .jump()
      .die()
      .exec()
      )
