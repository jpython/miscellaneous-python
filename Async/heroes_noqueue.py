#!/usr/bin/env python3

# to read:
# https://djangostars.com/blog/asynchronous-programming-in-python-asyncio/
# Merci à Julien pour l'inspiration et à Artiom pour le soutien moral

import asyncio
from functools import wraps
#from warnings import catch_warnings

class Hero:
    def __init__(self, name, prev=None):
        self.name   = name
        self.prev   = prev
        # How to avoid RunTimeWarning properly
        # (not masking it, idiot)
        #self.action = self._nothing()
        self.action = None

    def __stack_action(func):
        @wraps(func)
        def _(self, *args, **kwargs):
            self.action = func(self, *args, **kwargs)
            return Hero(self.name, prev=self)
        return _

    # so far useless...
    async def _nothing(self):
        pass 

    @__stack_action
    async def sleep(self, delay):
        print(f"{self.name} sleeping for {delay}s")
        await asyncio.sleep(delay)

    @__stack_action
    async def jump(self):
        print(f"{self.name} jumping")

    async def exec(self):
        if self.prev is not None:
            await self.prev.exec()
        # can be removed, using _nothing as a stub
        # but then issues RuntimeWarning for first
        # instanciation
        # RuntimeWarning: coroutine 'Hero._nothing' was never awaited
        if self.action is not None:
            await self.action

if __name__ == '__main__':
    from event_loop import Event_loop
    Event_loop(
     Hero('Mario')
      .jump()
      .sleep(3)
      .jump()
      .sleep(1)
      .jump()
      .jump()
      .sleep(2)
      .jump()
      .exec()
      ,
     Hero('Luigi')
      .sleep(2)
      .jump()
      .jump()
      .sleep(6)
      .jump()
      .jump()
      .exec()
      )
