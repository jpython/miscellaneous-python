#!/usr/bin/env python3
'''Boilerplate code for async execution of coroutines.'''

import asyncio

def Event_loop(*tasks):
    '''Wrap calls of coroutines in args in asyncio event loop.'''
    loop = asyncio.get_event_loop()
    loop.run_until_complete(asyncio.wait(tasks))
    loop.close

