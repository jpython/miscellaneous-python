#!/usr/bin/env python3

import asyncio
from functools import wraps 
import inspect

def chain_async_func(cls):
    async def _nothing(self):
        pass
    cls._nothing = _nothing
    cls.action   = _nothing(None)
    _init = cls.__init__
    def __init(self, *args, **kwargs):
        self.__init_args = (args, kwargs)
        self.prev = None
        _init(self, *args, **kwargs)
    cls.__init__ = __init
    def __stack_action(func):
        @wraps(func)
        def _(self, *args, **kwargs):
            self.action = func(self, *args, **kwargs)
            new = cls(*self.__init_args[0], **self.__init_args[1])
            new.prev = self
            return new
        return _
    for coro in inspect.getmembers(cls):
            #predicate=inspect.iscoroutinefunction):
        print('**coro', coro.__name__)
        cls.coro = __stack_action(coro)
        #setattr(cls, coro, __stack_action(getattr(cls,coro)))
    return cls

@chain_async_func
class Hero:

    def __init__(self, name):
        self.name  = name

    __stack__ = [ 'sleep', 'jump' ]

    async def sleep(self, delay):
        print(f"{self.name} sleeping for {delay}s")
        await asyncio.sleep(delay)

    async def jump(self):
        print(f"{self.name} jumping")

    async def exec(self):
        if self.prev is not None:
            await self.prev.exec()
        await self.action

loop = asyncio.get_event_loop()
tasks = [ Hero('Mario').jump().sleep(1).jump().exec() ]
loop.run_until_complete(asyncio.wait(tasks))
loop.close()
