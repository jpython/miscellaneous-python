#!/usr/bin/env python3

# to read:
# https://djangostars.com/blog/asynchronous-programming-in-python-asyncio/
# Merci à Julien pour l'inspiration et à Artiom pour le soutien moral

import asyncio

class Hero:
    def __init__(self, name):
        self.name = name
        self.queue = []

    async def _sleep(self, delay):
        print(f"{self.name} sleeping for {delay}s")
        await asyncio.sleep(delay)
    async def _jump(self):
        print(f"{self.name} is jumping")

    def sleep(self, delay):
        self.queue.append(self._sleep(delay))
        return self
    def jump(self):
        self.queue.append(self._jump())
        return self

    async def exec(self):
        # ou si on veut rejouer les actions
        # depuis le début :
        #for coro in self.queue:
        #    await coro
        while self.queue:
            await self.queue.pop(0)

if __name__ == '__main__':
    from event_loop import Event_loop
    Event_loop(
     Hero('Mario')
      .jump()
      .sleep(3)
      .jump()
      .sleep(1)
      .jump()
      .jump()
      .sleep(2)
      .jump()
      .exec()
      ,
     Hero('Luigi')
      .sleep(2)
      .jump()
      .jump()
      .sleep(6)
      .jump()
      .jump()
      .exec()
      ) 
