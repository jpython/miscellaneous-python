#!/usr/bin/env python3

# to read:
# https://djangostars.com/blog/asynchronous-programming-in-python-asyncio/
# Merci à Julien pour l'inspiration et à Artiom pour le soutien moral

import asyncio
from functools import wraps

class Hero:
    def __init__(self, name):
        self.name = name
        self.queue = []

    def __add_to_queue(func):
        @wraps(func)
        def _(self, *args, **kwargs):
            self.queue.append(func(self, *args, **kwargs))
            return self
        return _

    @__add_to_queue
    async def sleep(self, delay):
        print(f"{self.name} sleeping for {delay}s")
        await asyncio.sleep(delay)

    @__add_to_queue
    async def jump(self):
        print(f"{self.name} is jumping")

    async def exec(self):
        # ou si on veut rejouer les actions
        # depuis le début :
        #for coro in self.queue:
        #    await coro
        while self.queue:
            await self.queue.pop(0)

class Heroo:
    def __init__(self, name, prev=None):
        self.name = name
        self.prev = prev
        self.action = None
    async def _sleep(self, delay):
        print(f"{self.name} sleeping for {delay}s")
        await asyncio.sleep(delay)
    def sleep(self, delay):
        self.action = self._sleep(delay)
        return Heroo(self.name, prev=self)
    async def _jump(self):
        print(f"{self.name} jumping")
    def jump(self):
        self.action = self._jump()
        return Heroo(self.name, prev=self)
    async def exec(self):
        if self.prev is not None:
            await self.prev.exec()
        if self.action is not None:
            await self.action


class Herooo:
    def __init__(self, name, prev=None):
        self.name = name
        self.prev = prev
        self.action = None

    def __stack_action(func):
        def _(self, *args, **kwargs):
            self.action = func(self, *args, **kwargs)
            return Herooo(self.name, prev=self)
        return _
    
    @__stack_action
    async def sleep(self, delay):
        print(f"{self.name} sleeping for {delay}s")
        await asyncio.sleep(delay)
    @__stack_action
    async def jump(self):
        print(f"{self.name} jumping")
    async def exec(self):
        if self.prev is not None:
            await self.prev.exec()
        if self.action is not None:
            await self.action




if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    tasks = [
    ( Herooo('Mario')
      .sleep(1)
      .jump()
      .jump()
      .sleep(2)
      .jump()

      .exec()
      ),
    ]
    loop.run_until_complete(asyncio.wait(tasks))
    loop.close()
