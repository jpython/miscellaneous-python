#!/usr/bin/env python3

# TODO: si bug, faut pas partager le _nothing entre instance
# back to if action is not None:

import asyncio
from functools import wraps 

class Hero:
    async def _nothing(self):
        pass
    action = _nothing(None)

    def __init__(self, name, prev=None):
        self.name   = name
        self.prev   = prev

    def __stack_action(func):
        @wraps(func)
        def _(self, *args, **kwargs):
            self.action = func(self, *args, **kwargs)
            return Hero(self.name, prev=self)
        return _

    @__stack_action
    async def sleep(self, delay):
        print(f"{self.name} sleeping for {delay}s")
        await asyncio.sleep(delay)

    @__stack_action
    async def jump(self):
        print(f"{self.name} jumping")

    async def exec(self):
        #if self.action is None:
        #    self.action = self._nothing()
        if self.prev is not None:
            await self.prev.exec()
        await self.action

loop = asyncio.get_event_loop()
tasks = [ Hero('Mario').jump().sleep(1).jump().exec() ]
loop.run_until_complete(asyncio.wait(tasks))
loop.close()
