#!/usr/bin/env python3
'''Dispo sur le dépôt GIT:
https://framagit.org/jpython/miscellaneous-python
'''

from string import ascii_letters
# Usual Pythonic code:
def encode_char(c,n):
    start = ord('a') if c.islower() else \
            ord('A') if c.isupper() else \
            None
    # if c.islower():
    #     start = ord('a')
    # elif c.isupper():
    #     start = ord('A')
    # else:
    #     start = None
    return chr( ( ord(c) - start + n ) % 26 + start )

def encode_string(msg,n):
    return ''.join(( encode_char(c,n) if c in ascii_letters else c \
                                     for c in msg ) )

# One logical line: Haskell/LISP style!
encrypt_all_in_one = \
    (lambda msg,k: \
         ''.join(
             ( chr( (ord(c) - ord('a') + k) % 26 + ord('a') ) \
                   if c in ascii_lowercase \
                   else c \
                for c in msg )))

# LISP: Lot of Insipid and Stupid Parenthesis

decode_string = (lambda msg,n: encode_string(msg, -n))

def brute_force(crypted,hint):
    for key in range(1,26):
        cleartext = decode_string(crypted,key).lower()
        if hint.lower() in cleartext:
            break
    else:
        return (None, None)  # not found :'-(
    return (cleartext, key) # found :-)








if __name__ == '__main__':
    with open('crypted') as mystery:
        text = mystery.read().strip()
        clear, code = brute_force(text,'spam')
        if clear:
            print("Found! the key was: {}".format(code))
            print(clear)
        else:
            print("Not found, call Obélix...")

