#!/usr/bin/env python3
'''Published on repository: https://framagit.org/jpython/miscellaneous-python
'''

from string import ascii_letters

def encode_char(c,n):
    start = ord('a') if c.islower() else \
            ord('A') if c.isupper() else \
            None # or simply... c ! 
    return chr( ( ord(c) - start + n ) % 26 + start )

# to be continued ...
# 2 free coffees for the first to decipher the secret
# message...






def encode_string(msg,n):
    return ''.join( ( encode_char(c,n) if c in ascii_letters \
                                       else c for c in msg ) )

decode_string = (lambda msg,n: encode_string(msg,-n))

def brute_force(crypted,hint):
    found = False
    for code in range(1,26):
        cleartext = decode_string(crypted,code).lower()
        if hint.lower() in cleartext:
            found = True
            break
    return (cleartext, code) if found else (None, None)










if __name__ == '__main__':
    with open('crypted') as mystery:
        text = mystery.read().strip()
        clear, code = brute_force(text,'spam')
        if clear:
            print("Found! the key was: {}".format(code))
            print(clear)
        else:
            print("Not found, call Obélix...")

