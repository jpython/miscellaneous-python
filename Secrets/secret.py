
# using for usual statement 
def encrypt(cleartext, secret):
    encrypted = [] 
    for char in cleartext:
        encrypted.append(chr((ord(char) - ord('a') + secret) % 26 + ord('a')))
    return ''.join(encrypted)

# using comprehensions
def encrypt2(cleartext, secret):
    return ''.join(chr((ord(char) - ord('a') + secret ) % 26 + ord('a')) \
            for char in cleartext)

secret = 5
clear = 'thisistheclearmessage'
print('Secret:', secret)
print('Clear: ', clear)
print('Encrypted: ', encrypt(clear, 5))
print('Encrypted: ', encrypt2(clear, 5))
