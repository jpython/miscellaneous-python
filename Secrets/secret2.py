
import string

def encrypt_char(char, secret):
    '''Return a single character encoded by secret if ascii lowercase,
    unchanged if not'''
    return chr((ord(char) - ord('a') + secret ) % 26 + ord('a')) \
            if char in string.ascii_lowercase else char

# using for usual statement 
def encrypt(cleartext, secret):
    encrypted = [] 
    for char in cleartext:
        encrypted.append(encrypt_char(char, secret))
    return ''.join(encrypted)

# using comprehensions
def encrypt2(cleartext, secret):
    return ''.join( encrypt_char(c, secret) for c in cleartext)

secret = 5
clear = 'this is the clear message!'
print('Secret:', secret)
print('Clear: ', clear)
print('Encrypted: ', encrypt(clear, 5))
print('Encrypted: ', encrypt2(clear, 5))
