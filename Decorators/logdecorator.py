#!/usr/bin/env python3

from math import sqrt
from functools import wraps

def logit(the_func):
    @wraps(the_func)
    def _(*args,**kwargs):
        # to be return
        print('Function {} has been called'.format(the_func.__name__))
        print('args = {}'.format(args))
        print('kwargs = {}'.format(kwargs))
        result = the_func(*args,**kwargs)
        print('result = {}'.format(result))
        return result
    return _

@logit
def length(x,y):
    return sqrt(x**2 + y**2)

@logit
def cry_if_spam(msg):
    if 'spam' in msg:
        return 'I do not like spam!!!'
    else:
        return 'Good!'

print(length(2,3))

# print( logit(length)(2,3) )
# print( logit(length)(x=1,y=4))
# 
# length = logit(length)
# print(length(4,7))

print(cry_if_spam('ham egg bacon'))
print(cry_if_spam('ham spam egg bacon'))
