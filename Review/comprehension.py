#!/usr/bin/env python3

foods = 'spam ham egg sausage'.split()

# functional version
# 2 lines (one logical line) + SPEED (internal loop)
# shorter, easier too read, less error prone and more efficient!
results = [ food.upper() if food != 'spam' else \
            'BACON' for food in foods if food != 'egg' ]

print(results)
# Imperative version
# 7 lines !!!
results = []
for food in foods:
    if food != 'egg':
        if food != 'spam':
            results.append(food.upper())
        else:
            results.append('BACON')

print(results)
