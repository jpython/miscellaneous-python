#!/usr/bin/env python3
# This file is (will be) up to date on 
# https://framagit.org/jpython/miscellaneous-python
# and go to Review directory then

# Review on types, collections, tests and for loop

foods = [ 'spam', 'ham', 'egg', 'sausage', 'bacon' ] # a list
foodPrice = { 'spam':    12,     # a dictionary: keys->values
              'ham':     23,
              'egg':     11,
              'sausage': 15,
              'bacon':   21, } 

print("Here is what we are serving today: ", end='')
for food in foods:
    print(food, end=' ')
print() # line feed

print("1. Prices :")
for food, price in foodPrice.items():
    print('{:10}:\t {}'.format(food,price))

###########################################################
# What if we have food names and prices seperately ?

foods = [ 'spam', 'ham', 'egg', 'sausage', 'bacon' ] # a list
prices = [ 12, 23, 11, 15, 21  ] # another list

# zip(foods,prices) : [ ('spam',12), ('ham', 23) ... ]
# zip is transposition operator on matix, in a way
# >>> m = [ [1,2,3], [4,5,6], [7,8,9] ]
# >>> m
# [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
# >>> list(zip(m[0],m[1],m[2]))

print("2. Prices")
foodPrice = dict( zip(foods,prices) )
for food, price in foodPrice.items():
    print('{:10}:\t {}'.format(food,price))

# Reading from a simple file
foodPrice = {} # or dict()
with open('prices.txt') as inputfd:
    inputfd.readline() # ignore header line
    for line in inputfd:
        food, price = line.rstrip().split()
        foodPrice[food] = float(price) 

print("3. Prices")
foodPrice = dict( zip(foods,prices) )
for food, price in foodPrice.items():
    print('{:10}:\t {}'.format(food,price))

order = input("Your order: ").split() # a list
total = 0
for food in order:
    if food in foodPrice:
        total += foodPrice[food]
    else:
        print("Sorry, no {}!".format(food))
print('Price: ', total)

