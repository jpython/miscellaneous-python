#!/usr/bin/python3
import sys,re

usage = "Usage: {} pattern [file]\n".format(sys.argv[0])
if len(sys.argv) < 2:
	sys.stdout.write(usage)
	exit(1)

pattern = sys.argv[1]
input = open(sys.argv[2],'r') if len(sys.argv) > 2 else sys.stdin

blue = "\x1b[00;34;7m"
cyan = "\x1b[00;36;7m"
norm = "\x1b[00m" # man console_codes
green = "\x1b[00;32m"
red = "\x1b[00;31m"

for n,line in enumerate(input,1):
    line = line.rstrip()
    if not line or line.startswith('#'):
        continue
    line.replace('\t',' '*8)
    if re.search(pattern,line):
        line = re.sub('({})'.format(pattern),
		      r'{}\1{}'.format(blue,norm), line, count=1)
        print('{:2d}({}yes{}):{}'.format(n,green,norm,line))
    else:
        print('{:2d}({} no{}):{}'.format(n,red,norm,line))

# while(my $line = <>) {
# 	do { $.--; next; } if $line =~ m{^#};
# 	$line =~ s{\t}{        };
# 	chomp($line);
# 	print ' ' if $. < 10;
#         if ($line =~ m{$pattern}) {
#            $line =~ s{($pattern)}{$blue$1$norm};
#            print "$.(\e[00;32myes\e[00m):$line\n"; # green
# 	} else {
#            print "$.(\e[00;31m no\e[00m):$line\n"; # red
#         }
# }
