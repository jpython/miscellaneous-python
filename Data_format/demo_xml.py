#!/usr/bin/env python3

import xml.etree.ElementTree as ET

tree = ET.parse('movies.xml')

root = tree.getroot()

print("root.tag=",root.tag)

for child in root:
    print(child.tag, child.attrib)
    for child1 in child:
        print('\t',child1.tag, child1.attrib)

for desc in root.iter('description'):
    print(desc.text)
    print('-' * 10)
