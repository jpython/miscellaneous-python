#!/usr/bin/env python3

# Comment représenter élégament un déplacement de coordonnées
# discrètes en Python

from operator import add
from enum import Enum
from collections import namedtuple

### Première approche surcharger __radd__

class Move(Enum):
    UP    = ( 0 , -1 )
    DOWN  = ( 0 ,  1 )
    RIGHT = ( 1 ,  0 )
    LEFT  = (-1 ,  0 )
    def __radd__(self,coord):
        return tuple( add(*t) for t in zip( self.value, coord ) )

print( (3,4) + Move.UP )

### Autre approche: subclasser tuple

class Coord(tuple):
    def __add__(self,depl):
        return Coord( add(*t) for t in zip( self, depl.value) )
#    def __repr__(self):
#        return "Coord{}".format(super().__repr__())

c = Coord( (3,4) )
print( c + Move.UP )

## ou mieux encore namedtuple

class Coord2(namedtuple('Coord',['x','y'])):
    def __add__(self,depl):
        return Coord2( *(add(*t) for t in zip( self, depl.value )))

