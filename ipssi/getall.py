#!/usr/bin/env python3

from subprocess import call
import os

file = 'Listes/liste-git'

with open(file) as fd:
    for line in fd:
        name,url = line.rstrip().split(':',maxsplit=1)
        name = name.strip().replace(' ','_')
        url = url.strip()
        if not url.endswith('.git'):
            url = url + '.git'
        if not url.startswith('http'):
            print('No url for {} KO.'.format(name))
            continue
        os.mkdir(name)
        os.chdir(name)
        shutup = open(os.devnull,'wb')
        returncode = call(['git','clone',url],stdout=shutup,stderr=shutup)
        if returncode == 0:
            print('{} ok. {} cloné.'.format(name,url))
        else:
            print('git clone for {} KO.'.format(name))
        os.chdir('..')

