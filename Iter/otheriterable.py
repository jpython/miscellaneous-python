#!/usr/bin/env python3
'''Another simple iterator, return random numbers between min
and max, stop whenever max is obtained (and will not be returned).
This is typical "Duck Typing" (https://en.wikipedia.org/wiki/Duck_typing)
"If it walks like a duck and it quacks like a duck, then it must be a duck"
'''

from random import randint

class RandomValues:
    def __init__(self, min, max):
        self.min = min
        self.max = max
    def __iter__(self):
        return self
    def __next__(self):
        val = randint(self.min, self.max)
        if val == self.max:
            raise StopIteration # max never returned, stop here
        return val # sent back to next(...)

for i in RandomValues(1,10):
    print(i)
