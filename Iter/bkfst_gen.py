#!/usr/bin/env python3
'''Monty Python style breakfast generator'''

from random import random,choice

def bkfst_gen(spam=5,endp=0.3):
    '''Construct the list of ingredients
    with more spam.'''
    meals = 'ham egg bacon'.split() + [ 'spam' ] * spam
    while True:
        yield choice(meals) # pick and send back
        if random() < endp:
            return None # This will raise StopIteration

for i in range(10):
    spam = int(random()*5 + 1)
    print('Part de spam : {:.1f} %'.format(100*spam/(spam+3)))
    for i in range(3):
        print('  ', *bkfst_gen(endp=0.2, spam=spam))
        #print('  ',*[ m for m in bkfst_gen(endp=0.2,spam=spam) ])
