#!/usr/bin/env python3

# from random import random,choice

#def bkfst_gen(spam=5,endp=0.3):
#    '''Construct the list of ingredients
#    with more spam.'''
#    meals = 'ham egg bacon'.split() + [ 'spam' ] * spam
#    while True:
#        yield choice(meals) # pick and send back
#        if random() < endp:
#            return None # This will raise StopIteration

# sous forme de classe :

from random import random,choice

class BreakfastMenu:
    meals = 'ham egg bacon'.split()
    def __init__(self,spam=5,endp=0.3):
        self.meals = BreakfastMenu.meals + [ 'spam' ] * spam
        self.endp = endp
    def __iter__(self):
        return self
    def __next__(self):
        if hasattr(self,'food_sent') and random() < self.endp:
            raise StopIteration
        self.food_sent = True
        return choice(self.meals)

for i in range(10):
    spam = int(random()*5 + 1)
    print('Part de spam : {:.1f} %'.format(100*spam/(spam+3)))
    for i in range(3):
        print('  ', *BreakfastMenu(endp=0.2, spam=spam))

