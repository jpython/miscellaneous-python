class Truc:
    def __init__(self,start,end):
        self.state = start
        self.end   = end
    def __iter__(self):
        return self
    def __len__(self):
        return(self.end - self.state)
    def __next__(self):
        val = self.state
        self.state += 1
        if self.state > self.end:
            raise StopIteration
        return val

if __name__ == '__main__':
    for i in Truc(0,10):
        print(i) # 0 1 2 ... 9

