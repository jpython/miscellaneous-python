#!/usr/bin/env python3

text = input('Texte : ')

# pour comparer text et text[::-1]
# Python doit réaliser n comparaisons (n: longueur
# de la chaîne)

if text == text[::-1]:
    print('Test 1 : {} est un palindrome.'.format(text))

# Pour tester si c'est un palindrome c'est trop
# on peut s'arrêter au milieu ( partie entière(len/2) )
# radar 
# ra...    : 2 = int(5/2) comparaisons
# elle 
# el..     : 2 = int(4/2) comparaisons

def is_palindrome1(string):
    ans, n = False, len(string)
    for i in range(int(n/2)):
        if string[i] != string[n-1-i]:
            break
    else: # we are here if no break above
        ans = True
    return ans


# all( iterable ) : renvoie True si TOUS LES éléments
# sont True (s'arrête au premier False)
# ~i est l'inverse (__invert__) binaire d'un entier
# ~0 = -1, ~1 = -2, ~2 = -3 etc. (cf. représentation des
# entiers en complément à 2)

def is_palindrome2(string):
    return all( ( string[i] == string[~i]
                  for i in range(int(len(string)/2)) )
              )

if (is_palindrome1(text)):
    print('Test 2 : {} est un palindrome.'.format(text))

if (is_palindrome2(text)):
    print('Test 3 : {} est un palindrome.'.format(text))

