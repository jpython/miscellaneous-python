#!/usr/bin/env python3

# Devinez ce que fait le code suivant
# n'hésitez pas à lire la doc
# >>> import functools
# >>> help(functools.reduce)

# >>> from functools import reduce
# >>> reduce( (lambda a,b: a*b)  ,  [1,2,3,4,5,6] )
# 720
# >>> reduce( (lambda a,b: a*b)  ,  [1,2,3] )
# 6
# >>> reduce( (lambda a,b: a*b)  ,  [1,2] )
# 2

from functools import reduce
from itertools import chain
from operator import add

# [ (1,1) , 0, 1, 2, 3, ... (n-1) ] : départ
# [ (2,1) , 1, 2, 3, ... (n-1)    ] : step 1
# [ (3,2) , 2, 3, ... (n-1) ]       : step 2
# [ (5,3) , 2, 3, (n-1) ]           : step 3
# [ (8,5) , 3 ... ]
# FIBONACCI

def mystere(n):
    return reduce( ( lambda t,_: (add(*t),t[0]) ),
                   chain([ (1,1) ], range(n) ) 
                 )[0]

#n = int(input('Un nombre entier : '))
#print("(n+1)ème nombre de Fibonacci est",mystere(n))

def fibo_gen(a=1,b=1):
    while True:
        yield a
        a,b = b,a+b

fibo = fibo_gen()
print(*[ next(fibo) for i in range(10) ])

# remarque map et filter :
# LISPish style:
print(list(map( (lambda x: x**2),
           filter( ( lambda x: x%2 == 0), range(20) ) )
     ))
# [0, 4, 16, 36, 64, 100, 144, 196, 256, 324]
# Haskellian style:
print([ x**2 for x in range(20) if x%2 == 0 ])
# [0, 4, 16, 36, 64, 100, 144, 196, 256, 324]

