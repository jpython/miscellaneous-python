#!/usr/bin/env python3
# https://framagit.org/jpython/miscellaneous-python

import requests
import sys
from bs4 import BeautifulSoup

url = "http://www.leparisien.fr/meteo/ville/meteo-75"

with requests.get(url) as r:
    if r.ok:
        try:
            page = r.text
            soup = BeautifulSoup(page, 'html.parser')
            temp = soup.find('span',attrs={"class":"tempe"}).text
            realtemp = float(temp.rstrip('°C'))
            realplace = soup.find('td',attrs={"class":"ville-obs"}).strong.text
            weather = soup.select('td[class*="meteo-ephe"]')[0].attrs['class'][1]
            print("{}, température à Paris ({}) : {:.2f}°C".format(
                weather.title(),
                realplace,
                realtemp))
        except AttributeError:
            print("Zut, le html a encore changé.")
    else:
        sys.stderr.write('Pb de chargement de la page.\n')


