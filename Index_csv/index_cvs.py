#!/usr/bin/env python3

import csv
import pickle 

def icsvreader(fname,pos,*args,**kwargs):
    with open(fname) as fd:
        fd.seek(pos)
        while True:
            pos  = fd.tell()
            line = fd.readline()
            end  = fd.tell() 
            if line == '':
                return
            # TODO: essayer de ne pas reconstruire un reader à chaque ligne...
            data = next(csv.reader([ line.rstrip() ],*args,**kwargs))
            yield pos, end - pos, data

def saveindex(fname,data):
    with open(fname,'wb') as fd:
        pickle.dump(data,fd)

def readindex(fname):
    with open(fname,'rb') as fd:
        return pickle.load(fd)

def readfast(city_name,csv_file,index_file):
    # TODO : lire l'index, trouver la position, lire la ligne
    # décomposer la ligne
    pass

if __name__ == '__main__':
    fname = 'cities.csv'
    irdr = icsvreader(fname,0)
    for index,length,data in irdr:
        print(index,length,data)
    irdr = icsvreader(fname,0,delimiter=',')
    indexes = {}
    for index,length,(name,pop,area,gent) in irdr:
        indexes[name] = (index,length)
        print(index,length,name,pop,area,gent)
    saveindex('cities_index.pkl',indexes)

