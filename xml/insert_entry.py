#!/usr/bin/env python3

# pip install lxml bs4
# inspiration: https://zetcode.com/python/beautifulsoup/

from bs4 import BeautifulSoup

with open('sample.html') as f:

    contents = f.read()

    soup = BeautifulSoup(contents, 'lxml')

    print(soup.h2)
    print(soup.head)
    print(soup.li)

    # Modifying content
    tag = soup.find(text='Windows')
    tag.replace_with('Debian GNU/Linux')

    # A new list entry
    newtag = soup.new_tag('li')
    newtag.string='OpenBSD'

    # many ways to know where to insert
    # using div attributes or CSS selectors
    ultag = soup.ul

    ultag.append(newtag)

    print(soup.prettify())
