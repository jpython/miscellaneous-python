#!/usr/bin/env python3

class MyClass:
    defaultroot = '/tmp'
    def __init__(self,data,root=defaultroot):
        self.data = data
        self.root = root
    def __repr__(self):
        return "MyClass(data={},root={})".format(self.data,self.root)

