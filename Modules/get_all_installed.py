#!/usr/bin/env python3

import pkg_resources

def find_module(substring):
    installed_packages = [(d.project_name, d.version) for d in pkg_resources.working_set]
    return [ m for m in installed_packages if substring in m[0] ]
