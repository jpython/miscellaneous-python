#!/usr/bin/env python3

import requests

with requests.get('http://localhost:5000/api/list') as r:
    if r.ok:
        print(r.json()) # idem json.loads(r.content)
    else:
        print('Erreur: {}'.format(r.status_code))
        exit(1)
        
with requests.post('http://localhost:5000/api/add',
                   json={ 'Brest':'brestois' } ) as r:
    if r.ok:
        print('Brest ajouté !')
        
with requests.get('http://localhost:5000/api/get/Brest') as r:
    if r.ok:
        info = r.json() # idem json.dumps(r.content)
        print('Les habitants de {}' \
              ' sont les {}'.format('Brest',info))

