#!/usr/bin/env python3

data = [ 3.14, 2.71, 6.78, 4.67 ]

# Not very pythonic... 
i = 1
for elt in data:
    print("# {}\t{}".format(i,elt))
    i += 1

print("And now for something completely identical:")
# pythonic
for i, elt in enumerate(data,1):
    print("# {}\t{}".format(i,elt))

# including a dictionnary
foodPrice = { 'ham': 11, 'spam':5, 'egg':12 }

for i, (food, price) in enumerate(foodPrice.items(),1):
    print(i,food,price) 

