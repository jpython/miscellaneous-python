#!/usr/bin/env python3


from collections import namedtuple

# LA BASE DE DONNÉES
infoCity = { 'Lyon':(513275, 47.87, 'Lyonnais'),
             'Paris':(2206488, 105.40, 'Parisiens'),
             'Brest':(139163,49.51,'Brestois'),
             'Bordeaux':(249712,49.36,'Bordelais'),
             'Lille':(232_440,34.51,'Lillois')
          }

City = namedtuple('City',['name', 'pop', 'area', 'gent'])

myCities = [ City(name, *info) for name,info in infoCity.items() ]

myCities_dict = { name:City(name,*info) for name,info in infoCity.items() }

for city in myCities:
    print(city)
    print('Les habitants de {} sont les {}'.format(city.name,city.gent))
    print('Les habitants de {0.name} sont les {0.gent}'.format(city))
    print(f'Les habitants de {city.name} sont les {city.gent}')

for name,city in myCities_dict.items():
    print('{} -> {}'.format(name,city))

